#ifndef _NIKELMSP_H
#define _NIKELMSP_H

#define bic(arg, bit) (arg &= ~(bit))
#define bis(arg, bit) (arg |= (bit))
#define tgl(arg, bit) (arg ^= (bit))

#define LPM0_EINT _BIS_SR(LPM0_bits | GIE)
#define LPM1_EINT _BIS_SR(LPM1_bits | GIE)
#define LPM2_EINT _BIS_SR(LPM2_bits | GIE)
#define LPM3_EINT _BIS_SR(LPM3_bits | GIE)
#define LPM4_EINT _BIS_SR(LPM4_bits | GIE)

int main(void)
    __attribute__((noreturn));

#endif /* _NIKELMSP_H */
