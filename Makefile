all:

CPU=msp430g2553

rfid: CPU=msp430g2553
rfidusb: CPU=msp430f5510

include msp430.mk
-include config.mk

CFLAGS += -Wextra
CLEAN += rfid

all: rfid rfidusb
rfid: rfid.o
rfidusb: rfidusb.o
