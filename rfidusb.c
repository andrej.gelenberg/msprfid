#include <msp430f5510.h>

#include "likely.h"
#include "nikelmsp.h"

#define LED1 BIT0
#define LED2 BIT1
#define PLEDDIR P1DIR
#define PLEDOUT P1OUT

/* P4 */
#define SPI_CLK  BIT0
#define SPI_OUT  BIT1
#define SPI_IN   BIT2

#define UART_TX BIT4
#define UART_RX BIT5

static void
init_cpu() {

  P5SEL = BIT2 | BIT3; // enable crystal

  UCSCTL0 = 0;
  UCSCTL1 = 0;
  UCSCTL2 = 0;
  UCSCTL3 = SELREF__REFOCLK;
  UCSCTL4 = SELM__XT2CLK | SELS__XT2CLK | SELA__XT2CLK;
  UCSCTL5 = DIVPA__32; //DIVM__32 | DIVA__32 | DIVS__32;
  UCSCTL6 = XT2DRIVE0 | XT1OFF;

}

static void
init_ports() {

  PLEDDIR = BIT0 | LED2;
  P1SEL = BIT0;
  PLEDOUT = 0;

  P1DS = LED2;

}

int
main(void) {
  __dint();

  WDTCTL  = WDTPW | WDTHOLD;

  init_cpu();
  init_ports();

  while(1) {
    tgl(PLEDOUT, LED2);
  }


}
