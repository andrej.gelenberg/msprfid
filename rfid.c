/* Copyright (c) 2013 Andrej Gelenberg <andrej.gelenberg@udo.edu> */

#include <msp430g2553.h>

#include <stddef.h>
#include <stdint.h>

#include "likely.h"

#pragma GCC diagnostic ignored "-Wmain"

#define LPM0_EINT _BIS_SR(LPM0_bits | GIE)
#define LPM1_EINT _BIS_SR(LPM1_bits | GIE)
#define LPM2_EINT _BIS_SR(LPM2_bits | GIE)
#define LPM3_EINT _BIS_SR(LPM3_bits | GIE)
#define LPM4_EINT _BIS_SR(LPM4_bits | GIE)

#define LED1 BIT0

/* RFID 125 interface defines */
#define RESET BIT0
#define FOUND BIT1

#define SCK   BIT5
#define SDT   BIT6

/* UART defines */
#define UART_RX BIT1
#define UART_TX BIT2

/* == UART == */
#define uart_tx(msg) uart_tx_n(msg, sizeof(msg)-1)
static void
uart_tx_n(
    const char   *msg,
          size_t  len);
static void
uart_tx_byte(uint8_t byte);

static uint8_t
uart_tx_lock;

static const char *
uart_tx_buf;

static size_t
uart_tx_len;

static char *
spi_rx_buf;

static size_t
spi_rx_len;

#define spi_rx(buf) spi_rx_n(buf, sizeof(buf))
static void
spi_rx_n(
    char   *buf,
    size_t  len) {

  spi_rx_buf = buf;
  spi_rx_len = len;

  __dint();

  //uint8_t dummy = UCB0RXBUF;
  UC0IFG    &= ~(UCB0TXIFG);
  UCB0TXBUF  = 0;
  //UC0IE     |=  UCB0TXIE | UCB0RXIE;
  UC0IE     |=  UCB0RXIE;

  LPM3_EINT;

}

static void
uart_tx_n(
    const char   *msg,
          size_t  len) {

  while(1) {

    __dint();
    if ( likely(uart_tx_lock) ) {

      uart_tx_lock = 0;
      __eint();

      break;

    } else {

      LPM3_EINT;

    }

  }

  uart_tx_buf  = msg + 1;
  uart_tx_len  = len - 1;

  __dint();

  UCA0TXBUF    = *msg;
  UC0IE       |=  UCA0TXIE;
  LPM3_EINT;

}

static void
uart_tx_byte(uint8_t byte) {
  static const char alph[] =
    { '0', '1', '2', '3',
      '4', '5', '6', '7',
      '8', '9', 'A', 'B',
      'C', 'D', 'E', 'F' };

  char buf[2];

  buf[0] = alph[(byte>>4) & 0xf];
  buf[1] = alph[ byte     & 0xf];

  uart_tx_n(buf, sizeof(buf));

}

#pragma vector=USCIAB0RX_VECTOR
__interrupt void
usci_rx_isr(void) {

  P1OUT ^= LED1;

  *spi_rx_buf = UCB0RXBUF;

  ++spi_rx_buf;
  --spi_rx_len;

  if ( unlikely(!spi_rx_len) ) {

    UC0IE &= ~( UCB0RXIE | UCB0TXIE );
    UC0IFG &= ~UCB0TXIFG;
    LPM3_EXIT;

  } else {

    UCB0TXBUF = 0;
    UC0IFG &= ~(UCB0RXIFG | UCA0RXIFG);

  }

}

#pragma vector=USCIAB0TX_VECTOR
__interrupt void
usci_tx_usr(void) {

  if ( UC0IFG & UCA0TXIFG ) {

    if ( likely(uart_tx_len) ) {
  
      UCA0TXBUF = *uart_tx_buf;

      ++uart_tx_buf;
      --uart_tx_len;

    } else {

      uart_tx_lock  = 1;
      UC0IE        &= ~UCA0TXIE;

      LPM3_EXIT;

    }

    UC0IFG   &= ~UCA0TXIFG;

  }
  
  if ( UC0IFG & UCB0TXIFG ) {

    static uint8_t n = 0;
    n ^= 0xff;
    UCB0TXBUF = n;
    UC0IFG   &= ~UCB0TXIFG;

  }

}

/* == TIMER == */

#pragma vector=TIMER0_A0_VECTOR
__interrupt void
timer0_isr(void) {

  TACCTL0 &= ~CCIE;
  TACTL   &= ~(MC1 | MC0);
  TACTL   |= TACLR;

  LPM3_EXIT;

}

/* == PORT == */

#pragma vector=PORT2_VECTOR
__interrupt void
port2_isr() {

  if ( P2IFG & FOUND ) {

    P2IE  &= ~FOUND;
    P2IFG &= ~FOUND;
    LPM3_EXIT;

  } else {
    P2IFG = 0;
  }

}


/* == INIT FUNCTIONS == */

static void
init_cpu(void) {

  DCOCTL  = 0; /* lowest dco mode */
  BCSCTL1 = CALBC1_16MHZ;   /* calibartion */
  DCOCTL  = CALDCO_16MHZ;    /* set clock to 16 Mhz */
  BCSCTL2 = 0; /* MCLK -> DCOCLK, SMCLK -> DCOCLK,
                  DCO Resister -> internal */
  BCSCTL3 = 0; /* XT2Sx -> def, LFXT1 -> 32768 crystal,
                  XCAP ~ 1pF, XT2OF -> 0, LFX1OF -> 0 */

}

static void
init_usci(void) {

  /* UCA UART 9600 8n1 */

  UCA0CTL1 = UCSWRST | UCSSEL0 | UCSSEL1;
  /* Clock -> SMCLK, no error char, no break char,
     no dormant, next frame data, next not break,
     disable USCI */

  UCA0CTL0 = 0;
  /* LSB, 8n1, UART, Async*/

  /* 9600@16MHz */
  UCA0BR0  = (uint8_t)1666;
  UCA0BR1  = 1666 >> 8;
  UCA0MCTL = 6 << 1;

  UCA0CTL1     &= ~UCSWRST;

  uart_tx_lock = 1;

  /* UCB SPI 8bit */

  UCB0CTL1 = UCSSEL1 | UCSSEL0 | UCSWRST;
  /* SMCLK, reset */

  UCB0CTL0 = UCCKPH | UCCKPL | UCMSB | UCMST | UCSYNC;
  /* inactive clock hight, capture on first edge,
     MSB, 8bit, master, 3pin, sync */

  UCB0BR0  = 64;
  UCB0BR1  = 0;

  UCB0CTL1     &= ~UCSWRST;

}

static void
init_ports() {

  P1SEL  = UART_TX | UART_RX | SCK | SDT;
  P1SEL2 = UART_TX | UART_RX | SCK | SDT;

  P1REN = 0;
  P2REN = 0;

  P1OUT = 0;
  P2OUT = 0;

  P1DIR = LED1;
  P2DIR = RESET;

  P1IES = 0;
  P2IES = 0;

}

static void
init_timer(void) {

  TACTL  = TASSEL0 | TACLR;
  TACCR0 = 4;

}

/* == MAIN == */

void
main(void) {

  __dint();

  WDTCTL  = WDTPW | WDTHOLD;

  init_cpu();
  init_usci();
  init_ports();
  init_timer();

  __eint();

  static uint8_t
  tag_id[5];

  while (1) {

    P1OUT &= ~LED1;

    /* reset */
    __dint();
    TACCTL0  = CCIE;
    P2OUT   |= RESET;
    TACTL   |= MC0;
    LPM3_EINT;
    P2OUT   &= ~RESET;

    /* wait found signal */
    __dint();
    P2IE |= FOUND;
    LPM3_EINT;

    P1OUT |= LED1;

    /* get id */
    spi_rx(tag_id);

    uart_tx_byte(tag_id[0]);
    uart_tx_byte(tag_id[1]);
    uart_tx_byte(tag_id[2]);
    uart_tx_byte(tag_id[3]);
    uart_tx_byte(tag_id[4]);
    uart_tx("\r\n");

  }

}
